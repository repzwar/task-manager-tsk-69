package ru.pisarev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.pisarev.tm.model.AuthorizedUser;
import ru.pisarev.tm.service.ProjectService;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService service;

    @Secured({"ROLE_USER"})
    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user) {
        return new ModelAndView("project-list", "projects", service.findAll(user.getUserId()));
    }
}
