package ru.pisarev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.ProjectEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.ProjectAbstractListener;

@Component
public class ProjectClearListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        projectEndpoint.clearProject(getSession());
    }

}
